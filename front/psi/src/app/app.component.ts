import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {DataService} from "./services/data.service";
import {Observable} from "rxjs";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {Listing} from "./models/listing";
import {map} from "rxjs/operators";
import {BubbleChartDataItem, BubbleChartSeries} from "@swimlane/ngx-charts";
import {LinearRegression} from "./models/linear-regression";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AppComponent implements OnInit {
  title = 'psi';
  columnsToDisplay = ['id', 'title'];
  expandedElement: Listing | null = {};
  data$: Observable<Listing[]>;
  linearRegression: LinearRegression;
  olsRegression: LinearRegression;
  linearRegressionKeys = [];
  olsRegressionKeys = [];

  chartData: BubbleChartSeries[];
  view: any[] = [1000, 600];

  isLoading = false;
  colorScheme = {
    domain: ["#5AA454", "#E44D25", "#CFC0BB", "#7aa3e5", "#a8385d", "#aae3f5"]
  };


  constructor(private service: DataService, private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.data$ = this.service.pairList();
  }

  onCLick(element: Listing) {
    this.isLoading = true;
    this.chartData = null;
    if (this.expandedElement) {
      this.expandedElement = this.expandedElement === element ? null : element;
      this.cdr.detectChanges();
      this.loadChart(element);
      this.loadLinearRegression(this.expandedElement.title);
    }
  }

  loadChart(element: Listing) {
    this.service.pairByName(this.expandedElement.title)
      .pipe(
        map(resultList => {
          return <BubbleChartSeries>{
            name: element.title,
            series: resultList
              .filter((item, index) => index < 40)
              .map((item, index) => {
                // console.log(item);
                return <BubbleChartDataItem>{
                  name: index,
                  x: Number(item.x),
                  y: Number(item.y),
                  r: 2
                }
              })
          }
        })
      ).subscribe((res: BubbleChartSeries) => {
      console.log(element, res);
      this.chartData = [res];
      this.isLoading = false;
      this.cdr.detectChanges();
      // this.results = res
    });
  }

  onSelect(s) {
    console.log(s)
  }

  private loadLinearRegression(pair: string) {
     this.service.linearRegression(pair)
       .subscribe(res => {
         this.linearRegression = res;
         this.linearRegressionKeys = Object.keys(this.linearRegression);
       });
     this.service.olsRegression(pair)
       .subscribe(res => {
         this.olsRegression = res;
         this.olsRegressionKeys = Object.keys(this.olsRegression);
       });
  }
}
