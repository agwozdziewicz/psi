import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Result} from "../models/result";
import {Observable} from "rxjs";
import {Listing} from "../models/listing";
import {map} from "rxjs/operators";
import {LinearRegression} from "../models/linear-regression";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private readonly url = "http://localhost:8080/pairs";

  constructor(private http: HttpClient) { }

  pairList(): Observable<Listing[]> {
    return this.http.get<Listing[]>(this.url).pipe(
      map(list => list.map((el, index) => new Listing(el, index)))
    )
  }

  pairByName(pair: string): Observable<Result[]> {
    return this.http.get<Result[]>(`${this.url}/${pair}`)
  }

  linearRegression(pair: string): Observable<LinearRegression> {
    return this.http.get<LinearRegression>(`${this.url}/linear/${pair}`)
  }

  olsRegression(pair: string): Observable<LinearRegression> {
    return this.http.get<LinearRegression>(`${this.url}/ols/${pair}`)
  }
}
