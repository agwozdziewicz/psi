export interface LinearRegression {

  n?: string;
  slope?: string;
  slopeStdErr?: string;
  intercept?: string;
  sumOfCrossProducts?: string;
  regressionSumSquares?: string;
  slopeConfidenceInterval?: string;
  sumSquaredErrors?: string;
  xsumSquares?: string;
  r?: string;
  interceptStdErr?: string;
  meanSquareError?: string;
  rsquare?: string;
  significance?: string;
  totalSumSquares?: string;

}
