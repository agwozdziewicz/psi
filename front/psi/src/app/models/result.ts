export interface Result {

  personId?: string;
  id?: string;
  soundId?: string;
  soundName?: string;
  pictureId?: string;
  pictureName?: string;
  x?: string;
  y?: string

}
