package com.ag.ai;


import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReaderTest {

    @Test
    void testRegex() {
        Pattern pattern = Pattern.compile("^((?!_info\\.txt).)*$");

        assertFalse(pattern.matcher("C:\\git\\ai\\src\\main\\resources\\txt\\9076_2019_Apr_16_1300_info.txt").matches());
        assertTrue(pattern.matcher("C:\\git\\ai\\src\\main\\resources\\txt\\9076_2019_Apr_16_1300.txt").matches());
    }

}
