package com.ag.ai;

import com.ag.ai.models.Model;
import com.ag.ai.models.Picture;
import com.ag.ai.models.Sound;
import com.ag.ai.services.DataService;
import com.ag.ai.services.readers.CsvReader;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.Reader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.ClassLoader.getSystemResource;
import static java.nio.file.Files.newBufferedReader;
import static java.nio.file.Paths.get;

@SpringBootApplication
@RequiredArgsConstructor
public class AiApplication {

    private final CsvReader csvReader;
    private final DataService dataService;

    public static void main(String[] args) {
        SpringApplication.run(AiApplication.class, args);
    }

//    @Override
//    public void run(final String... args) throws Exception {
//        final URL soundSystemResource = getSystemResource("csv/IADS2.csv");
//        final URL pictureSystemResource = getSystemResource("csv/IAPS.csv");
//        final URL res = getSystemResource("csv/biostats.csv");
//        final Reader soundReader = newBufferedReader(get(soundSystemResource.toURI()));
//        final Reader pictureReader = newBufferedReader(get(pictureSystemResource.toURI()));
//
////        dataService.setSoundList(csvReader.readAll(soundReader)
////                .stream()
////                .map(Sound::map)
////                .collect(toList()));
////
////        dataService.setPictureList(csvReader.readAll(pictureReader)
////                .stream()
////                .map(Picture::map)
////                .collect(toList()));
////
//    }

//    public void run(final String... args) throws Exception {
//        final URL systemResource = getSystemResource("csv/IADS2.csv");
//        Reader reader = newBufferedReader(get(
//                systemResource.toURI()));
//        final List<Model> modelList = csvReader.readAll(reader)
//                .stream()
//                .map(Model::map)
//                .collect(Collectors.toList());
//
//        modelList.forEach(System.out::println);
//    }
}
