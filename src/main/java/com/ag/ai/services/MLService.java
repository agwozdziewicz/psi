package com.ag.ai.services;

import com.ag.ai.models.Result;
import lombok.RequiredArgsConstructor;
import org.apache.commons.math3.fitting.GaussianCurveFitter;
import org.apache.commons.math3.fitting.HarmonicCurveFitter;
import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;
import org.apache.commons.math3.stat.regression.GLSMultipleLinearRegression;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Double.valueOf;
import static java.util.stream.Stream.of;

@Service
@RequiredArgsConstructor
public class MLService {

    public SimpleRegression executeLinearRegression(final List<Result> results) {

        final SimpleRegression regression = new SimpleRegression();
        results.forEach(result -> regression.addData(valueOf(result.getX()), valueOf(result.getY())));

        System.out.println("\n\nLinear regression:");

        System.out.println(regression.getIntercept());
// displays intercept of regression line

        System.out.println(regression.getSlope());
// displays slope of regression line

        System.out.println(regression.getSlopeStdErr());

// displays slope standard error

        System.out.println(regression.predict(1.5d));
        return regression;
// displays predicted y value for x = 1.5

    }

    public OLSMultipleLinearRegression executeOLSMultipleLinearRegression(final List<Result> results) {
        OLSMultipleLinearRegression regression = new OLSMultipleLinearRegression();

        final int nobs = 5;
        final int nvars = 3;

        final double[] y = results.stream().map(Result::getY).mapToDouble(Double::valueOf).limit(nobs*(nvars+1)).toArray();
        regression.newSampleData(y, nobs, nvars);

        System.out.println("\n\nOLSMultipleLinearRegression:");

        double[] beta = regression.estimateRegressionParameters();
        System.out.println(Arrays.toString(beta));

        double[] residuals = regression.estimateResiduals();
        System.out.println(Arrays.toString(residuals));

        double[][] parametersVariance = regression.estimateRegressionParametersVariance();
        System.out.println(parametersVariance);

        double regressandVariance = regression.estimateRegressandVariance();
        System.out.println(regressandVariance);

        double rSquared = regression.calculateRSquared();
        System.out.println(rSquared);

        double sigma = regression.estimateRegressionStandardError();
        System.out.println(sigma);

        return regression;
    }

    public GLSMultipleLinearRegression executeGLSMultipleLinearRegression(final List<Result> results) {
        GLSMultipleLinearRegression regression = new GLSMultipleLinearRegression();

        final double[] y = results.stream().map(Result::getY).mapToDouble(Double::valueOf).limit(6).toArray();

        double[][] x = new double[6][];
        x[0] = new double[]{0, 0, 0, 0, 0};
        x[1] = new double[]{2.0, 0, 0, 0, 0};
        x[2] = new double[]{0, 3.0, 0, 0, 0};
        x[3] = new double[]{0, 0, 4.0, 0, 0};
        x[4] = new double[]{0, 0, 0, 5.0, 0};
        x[5] = new double[]{0, 0, 0, 0, 6.0};

        double[][] omega = new double[6][];
        omega[0] = new double[]{1.1, 0, 0, 0, 0, 0};
        omega[1] = new double[]{0, 2.2, 0, 0, 0, 0};
        omega[2] = new double[]{0, 0, 3.3, 0, 0, 0};
        omega[3] = new double[]{0, 0, 0, 4.4, 0, 0};
        omega[4] = new double[]{0, 0, 0, 0, 5.5, 0};
        omega[5] = new double[]{0, 0, 0, 0, 0, 6.6};

        regression.newSampleData(y, x, omega);
        System.out.println("\n\nGLSMultipleLinearRegression:");

        double[] beta = regression.estimateRegressionParameters();
        System.out.println(Arrays.toString(beta));

        double[] residuals = regression.estimateResiduals();
        System.out.println(Arrays.toString(residuals));

        double[][] parametersVariance = regression.estimateRegressionParametersVariance();
        System.out.println(parametersVariance);

        double regressandVariance = regression.estimateRegressandVariance();
        System.out.println(regressandVariance);

        double sigma = regression.estimateRegressionStandardError();
        System.out.println(sigma);

        return regression;
    }

    public PolynomialRegression executePolynominalRegression(final List<Result> results) {

        results.stream().map(Result::getX).toArray();
        System.out.println("\n\nPolynomialRegression :");

        final double[] y = results.stream().map(Result::getY).mapToDouble(Double::valueOf).toArray();
        final double[] x = results.stream().map(Result::getX).mapToDouble(Double::valueOf).toArray();

        PolynomialRegression regression = new PolynomialRegression(x, y, 3);
        System.out.println(regression.predict(10));

        return regression;
    }

    public PolynomialCurveFitter executePolynominalCurveFitter(final List<Result> results) {

        System.out.println("\n\nPolynomialCurveFitter:");

        final WeightedObservedPoints obs = new WeightedObservedPoints();
        results.forEach(result -> obs.add(valueOf(result.getX()), valueOf(result.getY())));

        final PolynomialCurveFitter fitter1 = PolynomialCurveFitter.create(1);
        final double[] coeff1 = fitter1.fit(obs.toList());
        System.out.println("coef [1] ="+ Arrays.toString(coeff1));

        final PolynomialCurveFitter fitter = PolynomialCurveFitter.create(2);
        final double[] coeff = fitter.fit(obs.toList());
        System.out.println("coef [2] ="+ Arrays.toString(coeff));

        final PolynomialCurveFitter fitter5 = PolynomialCurveFitter.create(3);
        final double[] coeff5 = fitter5.fit(obs.toList());
        System.out.println("coef [3] ="+ Arrays.toString(coeff5));

        final PolynomialCurveFitter fitter50 = PolynomialCurveFitter.create(5);
        final double[] coeff50 = fitter50.fit(obs.toList());
        System.out.println("coef [5] ="+ Arrays.toString(coeff50));

        return fitter;
    }

    public GaussianCurveFitter executeGaussianCurveFitter(final List<Result> results) {

        System.out.println("\n\nGaussianCurveFitter:");

        final WeightedObservedPoints obs = new WeightedObservedPoints();
        results.forEach(result -> obs.add(valueOf(result.getX()), valueOf(result.getY())));

        final GaussianCurveFitter fitter = GaussianCurveFitter.create();
        final double[] coeff = fitter.fit(obs.toList());
        System.out.println("coef="+ Arrays.toString(coeff));

        return fitter;
    }
    
    public HarmonicCurveFitter executeHarmonicCurveFitter(final List<Result> results) {

        System.out.println("\n\nHarmonicCurveFitter:");

        final WeightedObservedPoints obs = new WeightedObservedPoints();
        results.forEach(result -> obs.add(valueOf(result.getX()), valueOf(result.getY())));

        final HarmonicCurveFitter fitter = HarmonicCurveFitter.create();
        final double[] coeff = fitter.fit(obs.toList());
        System.out.println("coef="+ Arrays.toString(coeff));

        return fitter;
    }

}
