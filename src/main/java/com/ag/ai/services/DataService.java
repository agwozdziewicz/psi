package com.ag.ai.services;

import com.ag.ai.models.Person;
import com.ag.ai.models.Picture;
import com.ag.ai.models.Result;
import com.ag.ai.models.Sound;
import com.ag.ai.services.readers.CsvReader;
import com.ag.ai.services.readers.ResultReader;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.Collections.*;
import static java.util.stream.Collectors.groupingBy;

@Service
@RequiredArgsConstructor
public class DataService {

    public List<Person> personList;
    public List<Sound> soundList;
    public List<Picture> pictureList;

    private final ResultReader resultReader;
    private final CsvReader csvReader;

    @PostConstruct
    public void loadData() throws Exception {
        soundList = csvReader.loadSounds();
        pictureList = csvReader.loadPictures();
        personList = resultReader.execute();
        final Map<String, List<Result>> stringListMap1 = groupBySoundAndPicture();

    }

    public Set<String> pairList() {
        return groupBySoundAndPicture().keySet();
    }

    public List<Result> pairResult(String pair) {
        return groupBySoundAndPicture().entrySet()
                .stream()
                .filter(entry -> entry.getKey().equals(pair))
                .map(Map.Entry::getValue)
                .findAny()
                .orElse(emptyList());
    }

    Map<String, List<Result>> groupBySoundAndPicture() {
        return getResultStream()
                .sorted(Comparator.comparingInt(Result::getId))
                .collect(groupingBy(Result::soundPictureNamePair));
    }

    private Stream<Result> getResultStream() {
        return personList.stream()
                .map(Person::getResultSet)
                .flatMap(Collection::stream)
                .map(result -> {
                    this.soundList.stream()
                            .filter(sound -> sound.getId().equals(result.getSoundId()))
                            .findFirst()
                            .ifPresent(s -> result.setSoundName(s.getName()));

                    this.pictureList.stream()
                            .filter(picture -> picture.getId().equals(result.getPictureId()))
                            .findFirst()
                            .ifPresent(p -> result.setPictureName(p.getName()));

                    return result;
                });
    }


}
