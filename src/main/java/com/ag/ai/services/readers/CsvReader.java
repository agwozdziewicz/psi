package com.ag.ai.services.readers;

import com.ag.ai.models.Picture;
import com.ag.ai.models.Sound;
import com.opencsv.CSVReader;
import org.springframework.stereotype.Service;

import java.io.Reader;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.ClassLoader.getSystemResource;
import static java.nio.file.Files.newBufferedReader;
import static java.nio.file.Paths.get;
import static java.util.stream.Collectors.*;

@Service
public class CsvReader {

    public List<Sound> loadSounds() throws Exception {
        final URL systemResource = getSystemResource("csv/IADS2.csv");
        Reader reader = newBufferedReader(get(systemResource.toURI()));
        return readAll(reader)
                .stream()
                .map(line -> line[0].split(";"))
                .map(Sound::map)
                .collect(toList());
    }

    public List<Picture> loadPictures() throws Exception {
        final URL systemResource = getSystemResource("csv/IAPS.csv");
        Reader reader = newBufferedReader(get(systemResource.toURI()));
        return readAll(reader)
                .stream()
                .map(line -> line[0].split(";"))
                .map(Picture::map)
                .collect(toList());
    }

    public List<String[]> readAll(Reader reader) throws Exception {
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> list = csvReader.readAll();
        reader.close();
        csvReader.close();
        return list;
    }
}
