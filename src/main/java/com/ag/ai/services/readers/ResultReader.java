package com.ag.ai.services.readers;

import com.ag.ai.models.Person;
import com.ag.ai.models.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.Paths.get;
import static java.util.stream.Collectors.toList;

@Service
@Slf4j
@Scope()
public class ResultReader {

    private static final String NONE = "None";



    public List<Person> execute() throws IOException {
        Pattern pattern = Pattern.compile("^((?!_info\\.txt).)*$");

        return Files.walk(
                get("C:\\git\\ai\\src\\main\\resources\\txt"))
                .filter(file -> pattern.matcher(file.toString()).matches())
                .map(file -> parseFile(file))
                .filter(person -> person.getId() != null)
                .collect(toList());
    }


    public static Person parseFile(final Path file) {
        final Supplier<Stream<String>> lines = () -> readFile(file);

        final List<Result> collect = lines.get()
                .map(line -> line.split("\t"))
                .map(Result::map)
                .filter(result -> !result.getX().contains(NONE))
                .filter(result -> !result.getY().contains(NONE))
                .sorted(Comparator.comparingInt(Result::getId))
                .collect(Collectors.toList());

        return Person.builder()
                .id(lines.get()
                        .findFirst()
                        .map(line -> line.split("\t")[0])
                        .orElse(null))
                .resultSet(collect)
                .build();
    }

    private static Stream<String> readFile(final Path file) {
        try {
            return Files.lines(file);
        } catch (IOException e) {
            log.error("error for reading file {}", file.getFileName());
        }
        return Stream.empty();
    }
}
