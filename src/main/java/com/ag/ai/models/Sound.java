package com.ag.ai.models;

import lombok.Builder;
import lombok.Data;

import static java.lang.Double.*;

@Data
@Builder
public class Sound {

    private String name;
    private String id;
    private String ValenceMean;
    private String ValenceSD;
    private String ArousalMean;
    private String ArousalSD;

    public static Sound map(String[] el) {
        return builder()
                .name(el[0].trim())
                .id((el[1].trim()))
                .ValenceMean((el[2].trim()))
                .ValenceSD((el[3].trim()))
                .ArousalMean((el[4].trim()))
                .ArousalSD((el[5].trim()))
                .build();
    }

}
