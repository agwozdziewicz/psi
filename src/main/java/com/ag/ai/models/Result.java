package com.ag.ai.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.stream.Stream;

import static java.lang.Integer.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Stream.*;

@Slf4j
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Result {

    private Integer personId;
    private Integer id;
    private String soundId;
    private String soundName;
    private String pictureId;
    private String pictureName;
    private String x;
    private String y;

    public static Result map(String[] el) {
        final String personIdStr = el[0].trim();
        final String stringId = el[1].trim();
        Integer id;
        Integer personId = -1;
        try {
            personId = valueOf(personIdStr);
            id = valueOf(stringId);
        } catch (Exception e) {
            log.warn("wrong id {}", stringId);
            id = -1;
        }
        return builder()
                .personId(personId)
                .id(id)
                .soundId(el[4].trim())
                .pictureId(el[5].trim())
                .x(el[el.length - 2].trim())
                .y(el[el.length - 1].trim())
                .build();
    }

    public String soundPicturePair() {
        return of(soundId, pictureId).filter(StringUtils::hasText).collect(joining(" "));
    }

    public String soundPictureNamePair() {
        return of("soundName: " + soundName, "pictureName: " + pictureName).filter(StringUtils::hasText).collect(joining(" "));
    }
}
