package com.ag.ai.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Model {

    private String name;
    private String sex;
    private String age;
    private String height;
    private String weight;

    public static Model map(String[] el) {
        return builder()
                .name(el[0].trim())
                .sex(el[1].trim())
                .age(el[2].trim())
                .height(el[3].trim())
                .weight(el[4].trim())
                .build();
    }

}
