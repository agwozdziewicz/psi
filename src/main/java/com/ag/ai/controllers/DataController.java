package com.ag.ai.controllers;

import com.ag.ai.models.Person;
import com.ag.ai.models.Result;
import com.ag.ai.services.DataService;
import com.ag.ai.services.MLService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*", allowCredentials = "true", value = "*")
public class DataController {

    private final DataService dataService;
    private final MLService mlService;

    @GetMapping("data")
    public List<Person> getData() {
        return dataService.personList;
    }

    @GetMapping("pairs")
    public Set<String> pairList() {
        return dataService.pairList();
    }

    @GetMapping("pairs/{pair}")
    public List<Result> getByPair(@PathVariable(value = "pair") String pair) {
        final List<Result> results = dataService.pairResult(pair);

        mlService.executeLinearRegression(results);
        mlService.executePolynominalRegression(results);

        mlService.executeGLSMultipleLinearRegression(results);
        mlService.executeOLSMultipleLinearRegression(results);
        mlService.executePolynominalCurveFitter(results);
        mlService.executeGaussianCurveFitter(results);
        mlService.executeHarmonicCurveFitter(results);

        return results;
    }

    @GetMapping("pairs/linear/{pair}")
    public SimpleRegression linear(@PathVariable(value = "pair") String pair) {
        return mlService.executeLinearRegression(dataService.pairResult(pair));
    }

//    @GetMapping("pairs/ols/{pair}")
    public OLSMultipleLinearRegression ols(@PathVariable(value = "pair") String pair) {
//        mlService.executeGLSMultipleLinearRegression(dataService.pairResult(pair));
        return null;
//        return mlService.executeOLSMultipleLinearRegression(dataService.pairResult(pair));
    }

}
